package ca.cegepdrummond;
import java.util.Scanner;

public class Serie4_EntreesClavier {

    public void clavier1() {
        Scanner clavier = new Scanner(System.in);
        int num1 = clavier.nextInt();
        System.out.println(num1);
    }


    public void clavier2() {
        Scanner clavier = new Scanner(System.in);
        int num1 = clavier.nextInt();
        System.out.println(num1);
    }

    /*
     * Modifiez ce code afin qu'il demande une chaine de caractère suivit d'un entier
     * (ils peuvent être sur la même ligne ou sur deux lignes différentes).
     * Vous devez ensuite afficher la chaine de caractère sur la ligne et l'entier sur la ligne suivante.
     */
    public void clavier3() {

         Scanner clavier = new Scanner(System.in);
         System.out.println("Veuillez entrer une chaine de caractere avec un entrée");
         String chaine1 = clavier.nextLine();
         int num1 = clavier.nextInt();
         System.out.println(chaine1 );
         System.out.println(num1);

    }

    public void clavier4() {
        Scanner s = new Scanner(System.in);
        String mot1 = s.nextLine();
        String mot2 = s.nextLine();
        String mot3 = s.nextLine();
        System.out.println(mot3);
        System.out.println(mot2);
        System.out.println(mot1);
    }


    public void clavier5() {
        Scanner clavier = new Scanner(System.in);
        String mot1 = clavier.nextLine();
        String mot2 = clavier.nextLine();
        String mot3 = clavier.nextLine();
        String mot4 = clavier.nextLine();
        String mot5 = clavier.nextLine();
        System.out.println(mot1);
        System.out.println(mot2);
        System.out.println(mot3);
        System.out.println(mot4);
        System.out.println(mot5);
    }

    public void clavier6() {
        Scanner s = new Scanner(System.in);
        String valeur1 = s.next();
        String valeur2 = s.next();
        String intermediaire;
        intermediaire = valeur1;
        valeur1 = valeur2;
        valeur2 = intermediaire;
        System.out.println(valeur1 + " " + valeur2); // <<<< ne modifiez pas cette ligne

    }



}
