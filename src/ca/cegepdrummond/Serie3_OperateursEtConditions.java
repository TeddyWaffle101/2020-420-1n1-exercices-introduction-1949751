package ca.cegepdrummond;

public class Serie3_OperateursEtConditions {

    public void operateur1() {

        int a = 3;
        int b = 6;

        if (b % a == 6 % 3) {
            System.out.println("choix 1");
        } else {
            System.out.println("choix 2");
        }

    }

    public void operateur2() {

        int a = 3;
        int b = 6;
        int c = 2;

        if (b % a == c - b / a) {
            System.out.println("Réponse A");
        } else {
            System.out.println("Réponse B");
        }
        ;

    }


    public void operateur3() {
        int a = 3;
        int b = 6;
        int c = 2;

        if (!(b % a == c - b / a)) {
            System.out.println("Réponse A");
        } else {
            System.out.println("Réponse B");
        }
        ;
    }

    public void operateur4() {
        int a = 3;
        int b = 6;
        int c = 2;

        if (b % a != c - b / a) {
            System.out.println("Réponse A");
        } else {
            System.out.println("Réponse B");
        }
    }

    public void operateur5() {
        int a = 3;
        int b = 6;
        int c = 2;

        if ((a ^ b) % c == 8) {
            System.out.println("Réponse A");
        } else {
            System.out.println("Réponse B");
        }
    }

    public void operateur6() {

        int a = 4;
        int b = 5;
        int c = a + b;
        System.out.println("La somme est" + c);
    }

    public void condition1() {
        int a = 3;
        int b = 5;
        if (b > a) {
            System.out.println("bravo");
        }
    }

    public void condition2() {

        int a = 3;
        int b = 3;
        if (a < b) {
            System.out.println("choix 1");
        } else {
            System.out.println("choix 2");
        }
    }

    public void condition3() {
        int a = 5;
        int b = 4;
        if (a < b) {
            System.out.println("choix 1");
        } else if (a == b) {
            System.out.println("choix 2");
        } else {
            System.out.println("choix 3");
        }


    }

}
