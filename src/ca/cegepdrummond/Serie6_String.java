package ca.cegepdrummond;

import java.util.Scanner;

public class Serie6_String {
    /*
     * Modifiez ce code pour afficher la longueur de la chaine de caractères entrée.
     */
    public void string1() {
        Scanner s = new Scanner(System.in);
        String i = s.nextLine();
        System.out.println("La longueur est: " + i);
    }

    
    public void string2() {
        Scanner s = new Scanner(System.in);
        String s1 = s.nextLine();
        String s2 = s.nextLine();


        if( s1.equals(s2) ) {
            System.out.println("pareils");
        } else {
            System.out.println("différentes");
        }

    }

    /*
     * Modifiez le code afin de valider si les 2 chaines entrées sont pareils sans considérer les majuscules et minuscules.
     * Si elles sont égales, affichez "pareils".
     * Si non, affichez "différentes"
     *
     * Exemple:
     * allo
     * allo
     * affichera: pareils
     *
     * Exemple 2:
     * allo
     * ALLo
     * affichera: pareils...
     */
    public void string3() {
        Scanner s = new Scanner(System.in);
        String s1 = s.nextLine();
        String s2 = s.nextLine();


        if(s1 == (s2)) {
            System.out.println("pareils");
        } else {
            System.out.println("différentes");
        }
    }


    public void string4() {
        Scanner s = new Scanner(System.in);
        String s1 = s.nextLine();


        System.out.println("La chaine est "+s1+".");


    }

    /*
     * Modifiez le code afin de demander 2 chaines de caractères.
     * Vous devez ensuite afficher ces 2 chaines entre guillemet et mettre le caractère \ entres les deux.
     *
     * Exemples:
     * Allo
     * Hello
     * Affichera:
     * "Allo"\"Hello"
     */
    public void string5() {
        Scanner s = new Scanner(System.in);
        String s1 = s.nextLine();
        String s2 = s.nextLine();

        System.out.println(s1+"\""+s2);

    }

    public void string6() {
        Scanner s = new Scanner(System.in);
        System.out.println("Entrez un mot:");
        String chaine = s.nextLine();
        System.out.println("Entrez un nombre:");
        int position = s.nextInt();

        if (position >= 0 && position < -1) {  //<<< vérifiez si  longueur de la chaine
            System.out.println(+ chaine.charAt(position));  //<<<< affichez le caractère à la position demandée
        } else {
            System.out.println("invalide");

        }

    }

}
